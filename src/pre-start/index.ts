import path from 'path';
import dotenv from 'dotenv';
import commandLineArgs from 'command-line-args';

(() => {
    const options = commandLineArgs([
        {
            name: 'env',
            alias: 'e',
            defaultValue: 'development',
            type: String,
        },
    ]);
    const env = dotenv.config({
        path: path.join(path.parse(__dirname).dir.split('src').join(''), `env/${options.env}.env`),
    });
    if (env.error) {
        throw env.error;
    }
})();

