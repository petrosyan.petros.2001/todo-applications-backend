import morgan from "morgan";
import helmet from "helmet";

import express, { NextFunction, Request, Response } from "express";
import StatusCodes from "http-status-codes";
import "express-async-errors";
import logger from "jet-logger";
import { CustomError } from "@shared/helpers/errors.helper";
import { attachControllers } from "@decorators/express";
import { AuthController } from "@controllers/auth.controller";
import cors from "cors";

const app = express();

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

if (process.env.NODE_ENV === "production") {
  app.use(helmet());
}

app.use(
  (err: Error | CustomError, _: Request, res: Response, __: NextFunction) => {
    logger.err(err, true);
    const status =
      err instanceof CustomError ? err.status : StatusCodes.BAD_REQUEST;
      console.log(err);
    return res.status(status).json({
      error: err.message,
    });
  }
);

app.get("/", (_: Request, res: Response) => {
  res.json({
    statusCode: 404,
    message: "Cannot GET /",
    error: "Not Found",
  });
});

const apiRouter = express.Router();
attachControllers(apiRouter, [AuthController]);
app.use("/api/v1", apiRouter);

export default app;
