import { Users } from "@entities/user.entity"
import { DataSource } from "typeorm"

export const Connection = new DataSource({
    type: "mongodb",
    url: process.env.MONGO_URL,
    entities: [Users],
    useNewUrlParser: true,
    synchronize: true,
    logging: false,
})