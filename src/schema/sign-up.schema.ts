import Joi from "joi";
export const SignUpSchema = Joi.object().keys({
  userName: Joi.string().min(1).max(50).required(),
  email: Joi.string().email({ tlds: { allow: false } }),
  password: Joi.string().min(8).max(20),
});
