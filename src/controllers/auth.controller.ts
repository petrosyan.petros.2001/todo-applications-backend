import {  Controller, Post, Body, Response as Res } from "@decorators/express";
import { Inject } from "@decorators/di";
import { AuthService } from "@services/auth.service";
import  {Response} from "express";
import { SignUpBody } from "src/types/sign-up.body";
import { SignUpSchema } from "@schema/sign-up.schema";
import { BadRequestExeptions } from "@shared/helpers/errors.helper";

@Controller("/auth")
export class AuthController {
  constructor(
    @Inject(AuthService)
    private readonly _authService: AuthService
  ) {}

  @Post("/sign-in")
  signUp() {
    return ;
  }

  @Post("/sign-up")
  async signIn(@Body() body: SignUpBody, @Res() res: Response) {
    const {error} = SignUpSchema.validate(body);
    if (error){
      res.status(400);
      return new BadRequestExeptions(error)
    }
    const response = await this._authService.signUp(body);
    res.status(response.status);
    return response;
  }
  
}
