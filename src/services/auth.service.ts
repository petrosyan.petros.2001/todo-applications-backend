import { Injectable } from "@decorators/di";
import { Users } from "@entities/user.entity";
import { Connection } from "src/connection";
import { MongoRepository } from "typeorm";
import { ErrorEnum } from "@shared/constants/errors";
import {
  BadRequestExeptions,
  InitialServerExeptions,
} from "@shared/helpers/errors.helper";
import { Return } from "@shared/helpers/return.helper";
import { SignUpBody } from "src/types/sign-up.body";
import bcrypt from "bcrypt";

@Injectable()
export class AuthService {
  private async getRepository(): Promise<MongoRepository<Users>> {
    return await Connection.getMongoRepository(Users);
  }

  signIn() {
    return "asd";
  }

  async signUp(body: SignUpBody) {
    try {
      const userRepository = await this.getRepository();
      const exists = await userRepository.findBy({
        email: body.email,
      });
      if (!exists.length) {
        const hashPassword = await bcrypt.hashSync(body.password, 10);
        const entity = userRepository.create({
          ...body,
          password: hashPassword,
          createdAt: new Date(),
        });
        await userRepository.insertOne(entity);
        return Return.ok();
      } else {
        return new BadRequestExeptions(ErrorEnum.DUPLICATED_EMAIL);
      }
    } catch (e) {
      return new InitialServerExeptions();
    }
  }
}
