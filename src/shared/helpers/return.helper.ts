import HttpStatusCodes from "http-status-codes";

export class Return {
  static ok(response?: { message?: string }): {message: string, status: number} {
    return { status: HttpStatusCodes.CREATED, message: response?.message || 'ok' };
  }

  static returnData<T>(data: T): { data: T } {
    return { data };
  }
}
