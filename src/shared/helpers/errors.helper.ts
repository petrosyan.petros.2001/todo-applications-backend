import HttpStatusCodes from "http-status-codes";
import Joi from "joi";

export abstract class CustomError extends Error {
  public readonly status = HttpStatusCodes.BAD_REQUEST;
  public readonly message;
  constructor(message: string | Joi.ValidationError, status: number) {
    super();
    this.status = status;
    this.message = message
  }
}

export class BadRequestExeptions extends CustomError {
  public static readonly message = "Bad Request";
  public static readonly status = HttpStatusCodes.BAD_REQUEST;

  constructor(error?: string | Joi.ValidationError) {
    super(error || BadRequestExeptions.message, BadRequestExeptions.status);
  }
}

export class InitialServerExeptions extends CustomError {
    public static readonly message = "Initial Server Error";
    public static readonly status = HttpStatusCodes.INTERNAL_SERVER_ERROR;
  
    constructor() {
      super(InitialServerExeptions.message, InitialServerExeptions.status);
    }
  }
