export class SignUpBody {
    userName: string
    email: string
    password: string
}