import './pre-start';
import logger from 'jet-logger';
import server from './server';
import "reflect-metadata"
import { Connection } from "./connection"
import { print } from "@shared/helpers/print.helper";

const serverStartMsg = 'Express server started on port: ',
        port = (process.env.PORT || 3000);

Connection.initialize().then(() =>{
    console.clear();
    logger.info(serverStartMsg + port);
    server._router.stack.forEach(print.bind(null, []));   
    logger.info('MongoDB is Connected');
}).catch(e =>{
    logger.err(e.message);
})        

server.listen(port, () => {
    logger.info(serverStartMsg + port);
});
